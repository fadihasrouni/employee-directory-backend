# Employee Directory Backend

This project  is a spring boot REST webservice for employees management. It does not contain any User Interface. 
The project mainly consists of webservices that fetches and manipulates list of employees, countries and departments. Each employee works in a specific country inside a specific department.

  - Authenticated by username, password and token.
  - PostMan can be used to call the APIs locally for test.
  - Any front end application can consume this webservice.

# Requirements

For building and running the application you need:
 - JDK 1.8
 - Maven 3
 - Mysql database
 - Eclipse
 - Spring boot 2.1.7

# Importing/Building Project
Download/clone the source code and import as maven project in you IDE (preferrably eclipse). Once imported you should perform maven install, right click on your project -> Run As -> Maven Install, or perform it by command below.
```sh
$ mvn install
```
The application should build successfully and perform all JUnit test cases.

# Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the main method in the com.postlight.employeedirectory.EmployeeDirectoryApplication.java class from your IDE.

Alternatively you can use the Spring Boot Maven plugin like so:

```sh
$ mvn spring-boot:run
```

# Testing
The UI part can found under: https://employee-directory-frontend.herokuapp.com
WebService swagger documentation can be found under https://employee-management-backend.herokuapp.com/swagger-ui.html#/ (when deployed on your machine) if no change has been made to the application.
Postman JSON file is provided. It contains all the CRUD operation to be tested. 
Steps to a proper testing: 
- In PostMan variables change employee-service variable (in case you are NOT using localhost) as follows:
-- Click on settings icon -> Manage environments
-- Click on "local"
-- Edit "employee-service", insert new value "https://employee-management-backend.herokuapp.com"
- Register a new user by providing username and password. This can be found under user folder /register.
- Make sure you login first get a new TOKEN. This can be found under user folder /login.
- Write the generated token in the request header to be called next.
   Key = Authorization, Value = Token Generated

You can always perform JUnits for unit testing.

### Todos

 - Write MORE JUnit Tests to cover all units
 - Implement code checker for quality

### Version
1.1.1: Contains Add, View, Update, and Delete Employees, Countries and Departments (secured with JWT)

### Developers
Fadi Hasrouni(fadi.hasrouni@gmail.com)


**Free Software to use!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
