package com.postlight.employeedirectory.controller;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.postlight.employeedirectory.controller.response.DepartmentResponse;
import com.postlight.employeedirectory.security.JwtAuthenticationEntryPoint;
import com.postlight.employeedirectory.security.JwtTokenProvider;
import com.postlight.employeedirectory.service.DepartmentService;
import com.postlight.employeedirectory.service.impl.CustomUserDetailsService;

@WebMvcTest(DepartmentController.class)
@AutoConfigureMockMvc(addFilters = false)
public class DepartmentControllerTest
{
    @MockBean
    DepartmentService departmentService;
    
    @MockBean
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;
    
    @MockBean
    private JwtTokenProvider tokenProvider;

    @Autowired
    MockMvc mockMvc;

    DepartmentResponse validDepartment;

    @BeforeEach
    void setUp()
    {
	validDepartment = new DepartmentResponse();
	validDepartment.setId(1);
	validDepartment.setName("Software Department");
    }

    @AfterEach
    void tearDown()
    {
	reset(departmentService);
    }

    @Test
    void testfindDepartmentById() throws Exception
    {
	given(departmentService.findDepartmentById(any())).willReturn(validDepartment);

	MvcResult result = mockMvc.perform(get("/department/" + validDepartment.getId()))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validDepartment.getId())))
		.andExpect(jsonPath("$.name", equalTo("Software Department")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }
    
    @Test
    void testListAllDepartments() throws Exception
    {
	DepartmentResponse validDepartmentMarketing = new DepartmentResponse();
	validDepartmentMarketing.setId(2);
	validDepartmentMarketing.setName("Marketing");

	given(departmentService.listAllDepartments()).willReturn(Arrays.asList(validDepartment, validDepartmentMarketing));

	mockMvc.perform(get("/department/list"))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		
 		.andExpect(jsonPath("$[0].id", equalTo(1)))
	        .andExpect(jsonPath("$[0].name", equalTo("Software Department")))

	        .andExpect(jsonPath("$[1].id", equalTo(2)))
	        .andExpect(jsonPath("$[1].name", equalTo("Marketing")));
    }

    @Test
    void testSaveDepartment() throws Exception
    {
	given(departmentService.save(any())).willReturn(validDepartment);

	String departmentToSave = "{\"name\":\"Software Department\"}";

	MvcResult result = mockMvc
		.perform(post("/department/save").accept(MediaType.APPLICATION_JSON).content(departmentToSave)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validDepartment.getId())))
		.andExpect(jsonPath("$.name", equalTo("Software Department")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void testUpdateDepartment() throws Exception
    {
	given(departmentService.update(any())).willReturn(validDepartment);

	String departmentToSave = "{\"id\":\"1\",\"name\":\"Software Department\"}";

	MvcResult result = mockMvc
		.perform(post("/department/update").accept(MediaType.APPLICATION_JSON).content(departmentToSave)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validDepartment.getId())))
		.andExpect(jsonPath("$.name", equalTo("Software Department")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void testDeleteDepartment() throws Exception
    {
	MvcResult result = mockMvc.perform(delete("/department/delete/" + +validDepartment.getId()))
		.andExpect(status().isOk())
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }
}
