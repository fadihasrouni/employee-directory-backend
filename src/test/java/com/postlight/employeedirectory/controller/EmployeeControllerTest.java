package com.postlight.employeedirectory.controller;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Calendar;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.postlight.employeedirectory.controller.response.CountryResponse;
import com.postlight.employeedirectory.controller.response.DepartmentResponse;
import com.postlight.employeedirectory.controller.response.EmployeeResponse;
import com.postlight.employeedirectory.security.JwtAuthenticationEntryPoint;
import com.postlight.employeedirectory.security.JwtTokenProvider;
import com.postlight.employeedirectory.service.EmployeeService;
import com.postlight.employeedirectory.service.impl.CustomUserDetailsService;
import com.postlight.employeedirectory.utils.Gender;

@WebMvcTest(EmployeeController.class)
@AutoConfigureMockMvc(addFilters = false)
public class EmployeeControllerTest
{
    @MockBean
    EmployeeService employeeService;
    
    @MockBean
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;
    
    @MockBean
    private JwtTokenProvider tokenProvider;

    @Autowired
    MockMvc mockMvc;

    EmployeeResponse validEmployee;

    @BeforeEach
    void setUp()
    {
	validEmployee = new EmployeeResponse();
	validEmployee.setId(1);
	validEmployee.setName("Jhon");
	validEmployee.setFamilyName("Doe");
	validEmployee.setBirthDate(Calendar.getInstance().getTime());
	validEmployee.setCountry(new CountryResponse(1, "USA", "US"));
	validEmployee.setDepartment(new DepartmentResponse(1, "Software Department"));
	validEmployee.setEmail("jhon.doe@gmail.com");
	validEmployee.setGender(Gender.male);
	validEmployee.setHireDate(Calendar.getInstance().getTime());
	validEmployee.setJobTitle("Software Engineer");
	validEmployee.setPhoneNumber(71786706L);
	validEmployee.setPicture("/test/test.jpg");
	validEmployee.setSalary(3500);
    }
    
    @AfterEach
    void tearDown()
    {
	reset(employeeService);
    }
    
    @Test
    void testfindEmployeeById() throws Exception
    {
	given(employeeService.findEmployeeById(any())).willReturn(validEmployee);

	MvcResult result = mockMvc.perform(get("/employee/" + validEmployee.getId()))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validEmployee.getId())))
		.andExpect(jsonPath("$.name", equalTo("Jhon")))
		.andExpect(jsonPath("$.familyName", equalTo("Doe")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }
    
    @Test
    void testListAllEmployees() throws Exception
    {
	EmployeeResponse validEmployeeUSA = new EmployeeResponse();
	validEmployeeUSA.setId(5);
	validEmployeeUSA.setName("Fadi");
	validEmployeeUSA.setFamilyName("Hasrouni");
	validEmployeeUSA.setBirthDate(Calendar.getInstance().getTime());
	validEmployeeUSA.setCountry(new CountryResponse(1, "USA", "US"));
	validEmployeeUSA.setDepartment(new DepartmentResponse(1, "Software Department"));
	validEmployeeUSA.setEmail("jhon.doe@gmail.com");
	validEmployeeUSA.setGender(Gender.male);
	validEmployeeUSA.setHireDate(Calendar.getInstance().getTime());
	validEmployeeUSA.setJobTitle("Software Engineer");
	validEmployeeUSA.setPhoneNumber(71786706L);
	validEmployeeUSA.setPicture("/test/test.jpg");
	validEmployeeUSA.setSalary(3500);

	given(employeeService.listAllEmployees()).willReturn(Arrays.asList(validEmployee, validEmployeeUSA));

	mockMvc.perform(get("/employee/list"))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		
 		.andExpect(jsonPath("$[0].id", equalTo(1)))
 		.andExpect(jsonPath("$[0].name", equalTo("Jhon")))
		.andExpect(jsonPath("$[0].familyName", equalTo("Doe")))

	        .andExpect(jsonPath("$[1].id", equalTo(5)))
	        .andExpect(jsonPath("$[1].name", equalTo("Fadi")))
		.andExpect(jsonPath("$[1].familyName", equalTo("Hasrouni")));
    }

    @Test
    void testSaveEmployee() throws Exception
    {
	given(employeeService.save(any())).willReturn(validEmployee);

	String employeeToSave = "{\r\n" + 
		"	\"name\" : \"Jhon\",\r\n" + 
		"	\"familyName\" : \"Doe\",\r\n" + 
		"	\"email\" : \"Jhon.Doe@gmail.com\",\r\n" + 
		"	\"phoneNumber\" : 71786702,\r\n" + 
		"	\"picture\" : \"www.google.com/this/pic\",\r\n" + 
		"	\"jobTitle\" : \"software engineer\",\r\n" + 
		"	\"birthDate\" : \"1992-02-29\",\r\n" + 
		"	\"gender\" : \"female\",\r\n" + 
		"	\"hireDate\" : \"2015-07-25\",\r\n" + 
		"	\"salary\" : 3200,\r\n" + 
		"	\"department\" : 100, \r\n" + 
		"	\"country\" : 32\r\n" + 
		"}";

	MvcResult result = mockMvc
		.perform(post("/employee/save").accept(MediaType.APPLICATION_JSON).content(employeeToSave)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validEmployee.getId())))
		.andExpect(jsonPath("$.name", equalTo("Jhon")))
		.andExpect(jsonPath("$.familyName", equalTo("Doe")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void testUpdateEmployee() throws Exception
    {
	given(employeeService.update(any())).willReturn(validEmployee);

	String employeeToUpdate = "{\r\n" + 
		"        \"id\": 58,\r\n" + 
		"        \"name\": \"Jhon\",\r\n" + 
		"        \"familyName\": \"Doe\",\r\n" + 
		"        \"email\": \"Jhon.Doe@gmail.com\",\r\n" + 
		"        \"phoneNumber\": 71786702,\r\n" + 
		"        \"picture\": \"www.google.com/this/pic\",\r\n" + 
		"        \"jobTitle\": \"software engineer\",\r\n" + 
		"        \"birthDate\": \"0034-06-15T00:02:00.000+0000\",\r\n" + 
		"        \"gender\": \"male\",\r\n" + 
		"        \"hireDate\": \"0030-07-08T00:07:00.000+0000\",\r\n" + 
		"        \"salary\": 3200,\r\n" + 
		"        \"department\": 100,\r\n" + 
		"        \"country\":33\r\n" + 
		"    }";

	MvcResult result = mockMvc
		.perform(post("/employee/update").accept(MediaType.APPLICATION_JSON).content(employeeToUpdate)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validEmployee.getId())))
		.andExpect(jsonPath("$.name", equalTo("Jhon")))
		.andExpect(jsonPath("$.familyName", equalTo("Doe")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void testDeleteEmployee() throws Exception
    {
	MvcResult result = mockMvc.perform(delete("/employee/delete/" + +validEmployee.getId()))
		.andExpect(status().isOk())
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }

}
