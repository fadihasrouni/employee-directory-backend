package com.postlight.employeedirectory.controller;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.postlight.employeedirectory.controller.response.CountryResponse;
import com.postlight.employeedirectory.security.JwtAuthenticationEntryPoint;
import com.postlight.employeedirectory.security.JwtTokenProvider;
import com.postlight.employeedirectory.service.CountryService;
import com.postlight.employeedirectory.service.impl.CustomUserDetailsService;

@WebMvcTest(CountryController.class)
@AutoConfigureMockMvc(addFilters = false)
public class CountryControllerTest
{
    @MockBean
    CountryService countryService;
    
    @MockBean
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;
    
    @MockBean
    private JwtTokenProvider tokenProvider;
    
    @Autowired
    MockMvc mockMvc;

    CountryResponse validCountry;

    @BeforeEach
    void setUp()
    {
	validCountry = new CountryResponse();
	validCountry.setId(4);
	validCountry.setName("Lebanon");
	validCountry.setAlpha2("LB");
    }

    @AfterEach
    void tearDown()
    {
	reset(countryService);
    }

    @Test
    void testfindCountryById() throws Exception
    {
	given(countryService.findCountryById(any())).willReturn(validCountry);

	MvcResult result = mockMvc.perform(get("/country/" + validCountry.getId()))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validCountry.getId())))
		.andExpect(jsonPath("$.name", equalTo("Lebanon")))
		.andExpect(jsonPath("$.alpha2", equalTo("LB")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }
    
    @Test
    void testListAllCountries() throws Exception
    {
	CountryResponse validCountryUSA = new CountryResponse();
	validCountryUSA.setId(5);
	validCountryUSA.setName("USA");
	validCountryUSA.setAlpha2("US");

	given(countryService.listAllCountries()).willReturn(Arrays.asList(validCountry, validCountryUSA));

	mockMvc.perform(get("/country/list"))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		
 		.andExpect(jsonPath("$[0].id", equalTo(4)))
	        .andExpect(jsonPath("$[0].name", equalTo("Lebanon")))
		.andExpect(jsonPath("$[0].alpha2", equalTo("LB")))

	        .andExpect(jsonPath("$[1].id", equalTo(5)))
	        .andExpect(jsonPath("$[1].name", equalTo("USA")))
		.andExpect(jsonPath("$[1].alpha2", equalTo("US")));
    }

    @Test
    void testSaveCountry() throws Exception
    {
	given(countryService.save(any())).willReturn(validCountry);

	String countryToSave = "{\"id\":\"4\",\"name\":\"Lebanon\", \"alpha2\":\"LB\"}";

	MvcResult result = mockMvc
		.perform(post("/country/save").accept(MediaType.APPLICATION_JSON).content(countryToSave)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validCountry.getId())))
		.andExpect(jsonPath("$.name", equalTo("Lebanon")))
		.andExpect(jsonPath("$.alpha2", equalTo("LB")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void testUpdateCountry() throws Exception
    {
	given(countryService.update(any())).willReturn(validCountry);

	String countryToSave = "{\"id\":\"4\",\"name\":\"Lebanon\", \"alpha2\":\"LB\"}";

	MvcResult result = mockMvc
		.perform(post("/country/update").accept(MediaType.APPLICATION_JSON).content(countryToSave)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", equalTo(validCountry.getId())))
		.andExpect(jsonPath("$.name", equalTo("Lebanon")))
		.andExpect(jsonPath("$.alpha2", equalTo("LB")))
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void testDeleteCountry() throws Exception
    {
	MvcResult result = mockMvc.perform(delete("/country/delete/" + +validCountry.getId()))
		.andExpect(status().isOk())
		.andReturn();

	System.out.println(result.getResponse().getContentAsString());
    }
}
