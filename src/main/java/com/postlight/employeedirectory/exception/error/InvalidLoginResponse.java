package com.postlight.employeedirectory.exception.error;

public class InvalidLoginResponse
{
    private String username;
    private String password;
    private String message;

    public InvalidLoginResponse()
    {
	this.username = "Invalid Username";
	this.password = "Invalid Password";
	this.message = "Wrong username/password or incorrect token provided.";
    }

    public String getUsername()
    {
	return username;
    }

    public void setUsername(String username)
    {
	this.username = username;
    }

    public String getPassword()
    {
	return password;
    }

    public void setPassword(String password)
    {
	this.password = password;
    }

    public String getMessage()
    {
	return message;
    }

    public void setMessage(String message)
    {
	this.message = message;
    }

}