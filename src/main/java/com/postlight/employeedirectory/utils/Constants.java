package com.postlight.employeedirectory.utils;

public class Constants
{
    public static final String SIGN_UP_URLS = "/user/**";
    public static final String SWAGGER_UI = "/swagger-ui.html#/";
    public static final String SECRET ="SecretKeyToGenJWTs";
    public static final String TOKEN_PREFIX= "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final long EXPIRATION_TIME = 1200000; //20 mins
}
