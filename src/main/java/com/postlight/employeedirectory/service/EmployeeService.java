package com.postlight.employeedirectory.service;

import java.util.List;

import com.postlight.employeedirectory.controller.request.EmployeeRequest;
import com.postlight.employeedirectory.controller.response.EmployeeResponse;

public interface EmployeeService
{
    /**
     * Find employee by a given ID.
     * 
     * @param id
     * @return
     */
    public EmployeeResponse findEmployeeById(Integer id);
    
    /**
     * List all available employees
     * 
     * @return
     */
    public List<EmployeeResponse> listAllEmployees();
    
    /**
     * Saves a new employee
     * 
     * @param request
     * @return
     */
    public EmployeeResponse save(EmployeeRequest request);
    
    /**
     * Updates a given employee
     * 
     * @param request
     * @return
     */
    public EmployeeResponse update(EmployeeRequest request);
    
    /**
     * Delete a given employee
     * 
     * @param id
     */
    public void deleteEmployee(Integer id);

}	
