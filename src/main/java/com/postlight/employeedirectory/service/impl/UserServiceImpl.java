package com.postlight.employeedirectory.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.postlight.employeedirectory.controller.request.RegisterUserRequest;
import com.postlight.employeedirectory.controller.response.UserResponse;
import com.postlight.employeedirectory.exception.BadRequestException;
import com.postlight.employeedirectory.exception.UniqueConstraintException;
import com.postlight.employeedirectory.model.User;
import com.postlight.employeedirectory.repository.UserRepository;
import com.postlight.employeedirectory.service.UserService;

@Service
public class UserServiceImpl implements UserService
{
    private static final Logger log = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserResponse save(RegisterUserRequest userRequest)
    {
	// Populate the response
	UserResponse userResponse = new UserResponse();
	
	if(!userRequest.getPassword().equals(userRequest.getConfirmPassword()))
	{
	    Map<String, String> errorMap = new HashMap<String, String>();
	    errorMap.put("confirmPassword", "Password does not match with confirmed password");
	    throw new BadRequestException("Paswords provided does not match!", errorMap);
	}

	// Get existing user if present
	User existingUser = userRepository.findByUsername(userRequest.getUsername());

	// Check it user exists with same username (username must be unique)
	if (existingUser == null)
	{
	    // Make sure that password and confirmPassword match
	    User newUser = populateUserModel(userRequest);

	    newUser = userRepository.save(newUser);

	    userResponse = populateUserResponse(newUser);
	}
	else
	{
	    log.error("User already exists with username:" + userRequest.getUsername());
	    throw new UniqueConstraintException("User already exists with username=" + userRequest.getUsername());
	}

	return userResponse;
    }

    /**
     * Populates user response from user model
     * 
     * @param user
     * @return
     */
    private UserResponse populateUserResponse(User user)
    {
	UserResponse userResponse = new UserResponse();
	userResponse.setId(user.getId());
	userResponse.setUsername(user.getUsername());
	userResponse.setFullName(user.getFullName());
	userResponse.setPassword(user.getPassword());
	
	return userResponse;
    }

    /**
     * Popualtes user model from the user request
     * 
     * @param userRequest
     * @return
     */
    private User populateUserModel(RegisterUserRequest userRequest)
    {
	User user = new User();
	user.setId(userRequest.getId());
	user.setUsername(userRequest.getUsername());
	user.setFullName(userRequest.getFullName());
	user.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));

	return user;
    }

}
