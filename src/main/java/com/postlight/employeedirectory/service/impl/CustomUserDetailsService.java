package com.postlight.employeedirectory.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.postlight.employeedirectory.controller.response.UserResponse;
import com.postlight.employeedirectory.model.User;
import com.postlight.employeedirectory.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService
{
    
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
	User user = userRepository.findByUsername(username);
	if (user == null)
	{
	    throw new UsernameNotFoundException("User not found");
	}
	
	UserResponse userResponse = populateUserResponse(user);
	
	return userResponse;
    }
    
    @Transactional
    public UserResponse loadUserById(Long id)
    {
	User user = userRepository.findById(id);
	
	if (user == null)
	{
	    throw new UsernameNotFoundException("User not found");
	}
	
	UserResponse userResponse = populateUserResponse(user);
	return userResponse;
    }
    
    /**
     * Populates user response from user model
     * 
     * @param user
     * @return
     */
    private UserResponse populateUserResponse(User user)
    {
	UserResponse userResponse = new UserResponse();
	userResponse.setId(user.getId());
	userResponse.setUsername(user.getUsername());
	userResponse.setFullName(user.getFullName());
	userResponse.setPassword(user.getPassword());
	return userResponse;
    }
}
