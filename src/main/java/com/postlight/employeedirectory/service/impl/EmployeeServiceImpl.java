package com.postlight.employeedirectory.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.postlight.employeedirectory.controller.request.EmployeeRequest;
import com.postlight.employeedirectory.controller.response.CountryResponse;
import com.postlight.employeedirectory.controller.response.DepartmentResponse;
import com.postlight.employeedirectory.controller.response.EmployeeResponse;
import com.postlight.employeedirectory.exception.ResourceNotFoundException;
import com.postlight.employeedirectory.exception.UniqueConstraintException;
import com.postlight.employeedirectory.model.Employee;
import com.postlight.employeedirectory.repository.CountryRepository;
import com.postlight.employeedirectory.repository.DepartmentRepository;
import com.postlight.employeedirectory.repository.EmployeeRepository;
import com.postlight.employeedirectory.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService
{
    private static final Logger log = LogManager.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Autowired
    private CountryRepository countryRepository;
    
    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public EmployeeResponse findEmployeeById(Integer id)
    {
	EmployeeResponse result = new EmployeeResponse();

	// Call employee repository to find employee by ID
	Employee employee = employeeRepository.findById(id);

	if (employee != null)
	{
	    result = populateEmployeeResponse(employee);
	} else
	{
	    // If employee is null => no employee is found => an exception will be thrown
	    log.error("Couldn't find any employee with ID: " + id);
	    throw new ResourceNotFoundException("Couldn't find any employee with ID: " + id);
	}

	return result;
    }
    
    @Override
    public List<EmployeeResponse> listAllEmployees()
    {
	// Initialize empty list to be returned
	List<EmployeeResponse> employeesListResult = new ArrayList<EmployeeResponse>();

	// Get the list of all available employees 
	List<Employee> employees = employeeRepository.findAll();

	for (Employee employee : employees)
	{
	    EmployeeResponse employeeResponse = populateEmployeeResponse(employee);

	    employeesListResult.add(employeeResponse);
	}

	return employeesListResult;
    }

    @Override
    public EmployeeResponse save(EmployeeRequest request)
    {
	// Initializing response
	EmployeeResponse response = new EmployeeResponse();

	// Get existing Employee
	Employee existingEmployee = employeeRepository.findById(request.getId() != null ? request.getId() : -1);

	// Check it the Employee does not exists, insert
	if (existingEmployee == null)
	{
	    // Populate Employee model to be added/updated.
	    Employee employee = populateEmployeeModel(request, new Employee());

	    // Save function will save new employees and update old ones.
	    employee = employeeRepository.save(employee);
	    
	    log.info("Employee was added successfully in the database.");

	    // Populating response
	    response = populateEmployeeResponse(employee);
	} else
	{
	    throw new UniqueConstraintException("Employee with id=" + request.getId() + " already exists!");
	}

	return response;
    }
    
    @Override
    public EmployeeResponse update(EmployeeRequest request)
    {
	// Initializing response
	EmployeeResponse response = new EmployeeResponse();

	Employee existingEmployee = employeeRepository.findById(request.getId());

	if (existingEmployee != null)
	{
	    // Populate Employee model to be added/updated.
	    Employee employee  = existingEmployee;
	    
	    employee = populateEmployeeModel(request, employee);

	    // Save function will save new employees and update old ones.
	    employee = employeeRepository.save(employee);
	    
	    log.info("Employee was updated successfully in the database.");

	    // Populating response
	    response = populateEmployeeResponse(employee);
	} else
	{
	    throw new UniqueConstraintException("Employee with id=(" + request.getId() + ") does not exist to update!");
	}

	return response;
    }
    
    @Override
    public void deleteEmployee(Integer id)
    {
	Employee employee = new Employee();
	employee.setId(id);
	
	employeeRepository.delete(employee);
	
	log.info("Employee was deleted successfully from the database..");
    }

    /**
     * Populate EmployeeResponse from Employee Model
     * 
     * @param employee
     * @return
     */
    private EmployeeResponse populateEmployeeResponse(Employee employee)
    {
	// Populate EmployeeResponse from Employee Model
	EmployeeResponse employeeResponse = new EmployeeResponse();

	employeeResponse.setId(employee.getId());
	employeeResponse.setName(employee.getName());
	employeeResponse.setFamilyName(employee.getFamilyName());
	employeeResponse.setBirthDate(employee.getBirthDate());

	employeeResponse.setCountry(new CountryResponse(employee.getCountry()));
	employeeResponse.setDepartment(new DepartmentResponse(employee.getDepartment()));

	employeeResponse.setEmail(employee.getEmail());
	employeeResponse.setGender(employee.getGender());
	employeeResponse.setHireDate(employee.getHireDate());
	employeeResponse.setJobTitle(employee.getJobTitle());
	employeeResponse.setPhoneNumber(employee.getPhoneNumber());
	employeeResponse.setPicture(employee.getPicture());
	employeeResponse.setSalary(employee.getSalary());

	return employeeResponse;
    }

    /**
     * Populate Employee Model from EmployeeResponse
     * 
     * @param employeeRequest
     * @param existingEmployee
     * @return
     */
    private Employee populateEmployeeModel(EmployeeRequest employeeRequest, Employee existingEmployee)
    {
	// Populate Employee Model from EmployeeRequest. 
	//If the request is empty, it will get existing employee values (used for update)
	Employee employeeModel = new Employee();

	employeeModel.setId(employeeRequest.getId());
	employeeModel.setName(StringUtils.isBlank(employeeRequest.getName()) ? existingEmployee.getName() : employeeRequest.getName());
	employeeModel.setFamilyName(StringUtils.isBlank(employeeRequest.getFamilyName()) ? existingEmployee.getFamilyName() : employeeRequest.getFamilyName());
	employeeModel.setBirthDate(employeeRequest.getBirthDate() == null ? existingEmployee.getBirthDate() : employeeRequest.getBirthDate() );

	//The optimal case is not getting the employee and department by ID, they should be automatically extracted
	employeeModel.setCountry(countryRepository.findById(employeeRequest.getCountry() == null ? existingEmployee.getCountry().getId() : employeeRequest.getCountry()));
	employeeModel.setDepartment(departmentRepository.findById(employeeRequest.getDepartment() == null ? existingEmployee.getDepartment().getId() : employeeRequest.getDepartment()));

	employeeModel.setEmail(StringUtils.isBlank(employeeRequest.getEmail()) ? existingEmployee.getEmail() : employeeRequest.getEmail());
	employeeModel.setGender(employeeRequest.getGender() == null ? existingEmployee.getGender() : employeeRequest.getGender());
	employeeModel.setHireDate(employeeRequest.getHireDate() == null ? existingEmployee.getHireDate() : employeeRequest.getHireDate());
	employeeModel.setJobTitle(StringUtils.isBlank(employeeRequest.getJobTitle()) ? existingEmployee.getJobTitle() : employeeRequest.getJobTitle());
	employeeModel.setPhoneNumber(employeeRequest.getPhoneNumber() == null ? existingEmployee.getPhoneNumber() : employeeRequest.getPhoneNumber());
	employeeModel.setPicture(StringUtils.isBlank(employeeRequest.getPicture()) ? existingEmployee.getPicture() : employeeRequest.getPicture());
	employeeModel.setSalary(employeeRequest.getSalary() == 0 ? existingEmployee.getSalary() : employeeRequest.getSalary());

	return employeeModel;
    }

}
