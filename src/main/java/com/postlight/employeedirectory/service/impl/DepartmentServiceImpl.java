package com.postlight.employeedirectory.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.postlight.employeedirectory.controller.request.DepartmentRequest;
import com.postlight.employeedirectory.controller.response.DepartmentResponse;
import com.postlight.employeedirectory.exception.DeleteConstraintException;
import com.postlight.employeedirectory.exception.ResourceNotFoundException;
import com.postlight.employeedirectory.exception.UniqueConstraintException;
import com.postlight.employeedirectory.model.Department;
import com.postlight.employeedirectory.model.Employee;
import com.postlight.employeedirectory.repository.DepartmentRepository;
import com.postlight.employeedirectory.repository.EmployeeRepository;
import com.postlight.employeedirectory.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService
{
    private static final Logger log = LogManager.getLogger(DepartmentServiceImpl.class);

    @Autowired
    private DepartmentRepository departmentRepository;
    
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Override
    public DepartmentResponse findDepartmentById(Integer id) throws ResourceNotFoundException
    {
	DepartmentResponse result = new DepartmentResponse();

	// Call employee repository to find employee by ID
	Department department = departmentRepository.findById(id);

	if (department != null)
	{
	    result = populateDepartmentResponse(department);
	} else
	{
	    // If employee is null => no employee is found => an exception will be thrown
	    log.error("Couldn't find any department with ID: " + id);
	    throw new ResourceNotFoundException("Couldn't find any department with ID: " + id);
	}

	return result;
    }
    
    @Override
    public List<DepartmentResponse> listAllDepartments()
    {
	//Initialize empty list to be returned
	List<DepartmentResponse> departmentsListResult = new ArrayList<DepartmentResponse>();
	
	//Get the list of all departments available
	List<Department> departments = departmentRepository.findAll();
	
	for (Department department : departments)
	{
	    DepartmentResponse departmentReponse = populateDepartmentResponse(department);
	    
	    departmentsListResult.add(departmentReponse);
	}
	
	return departmentsListResult;
    }
    
    @Override
    public DepartmentResponse save(DepartmentRequest departmentRequest)
    {
	// Initializing response
	DepartmentResponse response = new DepartmentResponse();

	// Get existing department
	Department existingDepartment = departmentRepository
		.findById(departmentRequest.getId() != null ? departmentRequest.getId() : -1);

	// Check it the department does not exists, insert
	if (existingDepartment == null)
	{
	    // Populate Department model to be added/updated.
	    Department department = populateDepartmentModel(departmentRequest);

	    // Save function will save new departments and update old ones.
	    department = departmentRepository.save(department);
	    
	    log.info("Department was added successfully in the database.");

	    // Populating response
	    response = populateDepartmentResponse(department);
	} else
	{
	    throw new UniqueConstraintException("Department with id=" + departmentRequest.getId() + " already exists!");
	}

	return response;
    }

    @Override
    public DepartmentResponse update(DepartmentRequest departmentRequest)
    {
	// Initializing response
	DepartmentResponse response = new DepartmentResponse();

	// Get existing department
	Department existingDepartment = departmentRepository
		.findById(departmentRequest.getId() != null ? departmentRequest.getId() : -1);

	// Check it the department does not exists, insert
	if (existingDepartment != null)
	{
	    // Populate Department model to be added/updated.
	    Department department = populateDepartmentModel(departmentRequest);

	    // Save function will save new departments and update old ones.
	    department = departmentRepository.save(department);
	    
	    log.info("Department was updated successfully in the database.");

	    // Populating response
	    response = populateDepartmentResponse(department);
	} else
	{
	    throw new UniqueConstraintException("Department with id=" + departmentRequest.getId() + " does not exist to update!");
	}

	return response;
    }

    @Override
    public void deleteDepartment(Integer id)
    {
	Department department = new Department();
	department.setId(id);
	
	Employee employee = employeeRepository.findByDepartment(department);

	if (employee == null)
	{
	    departmentRepository.delete(department);	
	}
	else
	{
	    log.error("Couldn't delete department. The department has a foreign key in the employee table");
	    throw new DeleteConstraintException("Couldn't delete department. Please make sure no employee is registered in this department.");
	}
	
	log.info("Department was deleted successfully from the database.");
    }

    
    /**
     * Populate Department model from DepartmentRequest
     * 
     * @param departmentRequest
     * @return
     */
    private Department populateDepartmentModel(DepartmentRequest departmentRequest)
    {
	//Populate Department model from DepartmentRequest
	Department department = new Department();
	department.setId(departmentRequest.getId());
	department.setName(departmentRequest.getName());
	
	return department;
    }
    
    /**
     * Populate DepartmentResponse from Department Model
     * 
     * @param department
     * @return
     */
    private DepartmentResponse populateDepartmentResponse(Department department)
    {
	//Populate DepartmentResponse from Department Model
	DepartmentResponse departmentResponse = new DepartmentResponse();
	
	departmentResponse.setId(department.getId());
	departmentResponse.setName(department.getName());
 	
	return departmentResponse;
    }
}
