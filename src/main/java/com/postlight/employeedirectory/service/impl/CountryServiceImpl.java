package com.postlight.employeedirectory.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.postlight.employeedirectory.controller.request.CountryRequest;
import com.postlight.employeedirectory.controller.response.CountryResponse;
import com.postlight.employeedirectory.exception.DeleteConstraintException;
import com.postlight.employeedirectory.exception.ResourceNotFoundException;
import com.postlight.employeedirectory.exception.UniqueConstraintException;
import com.postlight.employeedirectory.model.Country;
import com.postlight.employeedirectory.model.Employee;
import com.postlight.employeedirectory.repository.CountryRepository;
import com.postlight.employeedirectory.repository.EmployeeRepository;
import com.postlight.employeedirectory.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService
{
    private static final Logger log = LogManager.getLogger(CountryServiceImpl.class);
    
    @Autowired
    private CountryRepository countryRepository;
    
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public CountryResponse findCountryById(Integer id) throws ResourceNotFoundException
    {
	CountryResponse result = new CountryResponse();

	//Call country repository to find country by ID
	Country country = countryRepository.findById(id);

	if (country != null)
	{
	    result = populateCountryResponse(country);
	}
	else
	{
	    //If country is null => no country is found => an exception will be thrown
	    log.error("Couldn't find any country with ID: " + id);
	    throw new ResourceNotFoundException("Couldn't find any country with ID: " + id);
	}
	
	return result;
    }

    @Override
    public CountryResponse save(CountryRequest countryRequest)
    {
	//Initializing response
	CountryResponse response  = new CountryResponse();
	
	//Get existing country
	Country existingCountry = countryRepository.findByAlpha2(countryRequest.getAlpha2());
	
	// Check it the country does not exists, insert
	if (existingCountry == null)
	{
	    // Populate Country model to be added/updated.
	    Country country = populateCountryModel(countryRequest);

	    // Save function will save new countries and update old ones.
	    country = countryRepository.save(country);
	    
	    log.info("Country was added successfully in the database..");

	    // Populating response
	    response = populateCountryResponse(country);
	} else
	{
	    throw new UniqueConstraintException("Country with "+countryRequest.getAlpha2()+" Alpha 2 Code already exists!");
	}	
	
	return response;
    }
    
    @Override
    public CountryResponse update(CountryRequest countryRequest)
    {
	//Initializing response
	CountryResponse response  = new CountryResponse();
	
	//Get existing country
	Country existingCountry = countryRepository.findById(countryRequest.getId());

	// Check it the country already exists, update
	if (existingCountry != null && countryRequest.getId() == existingCountry.getId())
	{
	    // Populate Country model to be added/updated.
	    Country country = populateCountryModel(countryRequest);

	    // Save function will save new countries and update old ones.
	    country = countryRepository.save(country);

	    log.info("Country was updated successfully in the database.");
	    
	    // Populating response
	    response = populateCountryResponse(country);
	} else
	{
	    throw new UniqueConstraintException("Country with id=(" + countryRequest.getId() + ") does not exist to update!");
	}	
	
	return response;
    }
    
    @Override
    public List<CountryResponse> listAllCountries()
    {
	//Initialize empty list to be returned
	List<CountryResponse> countriesListResult = new ArrayList<CountryResponse>();
	
	//Get the list of all countries available
	List<Country> countries = countryRepository.findAll();
	
	for (Country country : countries)
	{
	    CountryResponse countryReponse = populateCountryResponse(country);
	    
	    countriesListResult.add(countryReponse);
	}
	
	return countriesListResult;
    }
    
    @Override
    public void deleteCountry(Integer id) 
    {
	Country country = new Country();
	country.setId(id);

	Employee employee = employeeRepository.findByCountry(country);

	if (employee == null)
	{
	    countryRepository.delete(country);
	}
	else
	{
	    log.error("Couldn't delete country. The country has a foreign key in the employee table");
	    throw new DeleteConstraintException("Couldn't delete country. Please make sure no employee is registered in this country.");
	}

	log.info("Country was deleted successfully from the database.");
    }
    
    /**
     * Populate the Country model class from CountryRequest
     * 
     * @param countryRequest
     * @return
     */
    private Country populateCountryModel(CountryRequest countryRequest)
    {
	//Populate Country model from CountryRequest
	Country country = new Country();
	country.setId(countryRequest.getId());
	country.setName(countryRequest.getName());
	country.setAlpha2(countryRequest.getAlpha2());
	
	return country;
    }
    
    /**
     * Populate CountryReponse from Country Model
     * 
     * @param country
     * @return
     */
    private CountryResponse populateCountryResponse(Country country)
    {
	//Populate CountryReponse from Country Model
	CountryResponse countryResponse = new CountryResponse();
	
	countryResponse.setId(country.getId());
	countryResponse.setName(country.getName());
	countryResponse.setAlpha2(country.getAlpha2());
	
	return countryResponse;
    }

}
