package com.postlight.employeedirectory.service;

import java.util.List;

import com.postlight.employeedirectory.controller.request.CountryRequest;
import com.postlight.employeedirectory.controller.response.CountryResponse;
import com.postlight.employeedirectory.exception.ResourceNotFoundException;

/**
 * This layer is created to handle all the logic needed after getting the data
 * 
 * @author Fadi Hasrouni
 *
 */
public interface CountryService
{
   /**
    * Find country by a given ID.
    * 
    * @param id
    * @return
    * @throws ResourceNotFoundException
    */
    public CountryResponse findCountryById(Integer id) throws ResourceNotFoundException;
    
    /**
     * Saves a given country
     * 
     * @param country
     * @return
     */
    public CountryResponse save(CountryRequest country);
    
    /**
     * Updates a given country
     * 
     * @param country
     * @return
     */
    public CountryResponse update(CountryRequest country);
    
    
    /**
     * List all available countries
     * 
     * @return
     */
    public List<CountryResponse> listAllCountries();
    
    /**
     * Delete a given country
     * 
     * @param id
     */
    public void deleteCountry(Integer id);
}
