package com.postlight.employeedirectory.service;

import java.util.List;

import com.postlight.employeedirectory.controller.request.DepartmentRequest;
import com.postlight.employeedirectory.controller.response.DepartmentResponse;
import com.postlight.employeedirectory.exception.ResourceNotFoundException;

/**
 * This layer is created to handle all the logic needed after getting the data
 * 
 * @author Fadi Hasrouni
 *
 */
public interface DepartmentService
{
    /**
     * Find department by a given ID.
     * 
     * @param id
     * @return
     * @throws ResourceNotFoundException
     */
     public DepartmentResponse findDepartmentById(Integer id) throws ResourceNotFoundException;
     
     /**
      * Saves a given department
      * 
      * @param department
      * @return
      */
     public DepartmentResponse save(DepartmentRequest departmentRequest);
     
     /**
      * Updates a given department
      * 
      * @param department
      * @return
      */
     public DepartmentResponse update(DepartmentRequest departmentRequest);
     
     
     /**
      * List all available departments
      * 
      * @return
      */
     public List<DepartmentResponse> listAllDepartments();
     
     /**
      * Delete a given department
      * 
      * @param id
      */
     public void deleteDepartment(Integer id);
     
}
