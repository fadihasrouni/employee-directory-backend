package com.postlight.employeedirectory.service;

import com.postlight.employeedirectory.controller.request.RegisterUserRequest;
import com.postlight.employeedirectory.controller.response.UserResponse;

public interface UserService
{
    /**
     * Saves a given user
     * 
     * @param userRequest
     * @return
     */
    public UserResponse save(RegisterUserRequest userRequest);

}
