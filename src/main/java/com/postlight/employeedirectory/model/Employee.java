package com.postlight.employeedirectory.model;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.postlight.employeedirectory.utils.Gender;

@Entity
public class Employee
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotBlank(message = "Employee name is required")
    private String name;
    private String familyName;
    @NotBlank(message = "Employee email is required")
    private String email;
    private Long phoneNumber;
    private String picture;
    @NotBlank(message = "Employee job title is required")
    private String jobTitle;
    @JsonFormat(pattern="dd-mm-yyy")
    private Date birthDate;
    private Gender gender;
    @NotNull(message = "Employee hire date is required")
    @JsonFormat(pattern="dd-mm-yyy")
    private Date hireDate;
    private float salary;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Country country;
    
    @CreationTimestamp
    private LocalDateTime created_at;
    @UpdateTimestamp
    private LocalDateTime updated_at;

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public String getFamilyName()
    {
	return familyName;
    }

    public void setFamilyName(String familyName)
    {
	this.familyName = familyName;
    }

    public String getEmail()
    {
	return email;
    }

    public void setEmail(String email)
    {
	this.email = email;
    }

    public Long getPhoneNumber()
    {
	return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber)
    {
	this.phoneNumber = phoneNumber;
    }

    public String getPicture()
    {
	return picture;
    }

    public void setPicture(String picture)
    {
	this.picture = picture;
    }

    public String getJobTitle()
    {
	return jobTitle;
    }

    public void setJobTitle(String jobTitle)
    {
	this.jobTitle = jobTitle;
    }

    public Date getBirthDate()
    {
	return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
	this.birthDate = birthDate;
    }

    public Gender getGender()
    {
	return gender;
    }

    public void setGender(Gender gender)
    {
	this.gender = gender;
    }

    public Date getHireDate()
    {
	return hireDate;
    }

    public void setHireDate(Date hireDate)
    {
	this.hireDate = hireDate;
    }

    public float getSalary()
    {
	return salary;
    }

    public void setSalary(float salary)
    {
	this.salary = salary;
    }

    public Department getDepartment()
    {
	return department;
    }

    public void setDepartment(Department department)
    {
	this.department = department;
    }

    public Country getCountry()
    {
	return country;
    }

    public void setCountry(Country country)
    {
	this.country = country;
    }
    
    public LocalDateTime getCreated_at()
    {
	return created_at;
    }

    public void setCreated_at(LocalDateTime created_at)
    {
	this.created_at = created_at;
    }

    public LocalDateTime getUpdated_at()
    {
	return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at)
    {
	this.updated_at = updated_at;
    }
    
    @Override
    public String toString()
    {
       //Used for logging to be able to show the variables values
       return ReflectionToStringBuilder.toString(this, SHORT_PREFIX_STYLE);
    }
}
