package com.postlight.employeedirectory.model;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class Country
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    private String name;
    @Column(unique = true)
    private String alpha2;

    @CreationTimestamp
    private LocalDateTime created_at;
    @UpdateTimestamp
    private LocalDateTime updated_at;

    public Country()
    {

    }
    
    public Country(Integer id)
    {
	this.id = id;
    }
    
    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public String getAlpha2()
    {
	return alpha2;
    }

    public void setAlpha2(String alpha2)
    {
	this.alpha2 = alpha2;
    }

    public LocalDateTime getCreated_at()
    {
	return created_at;
    }

    public void setCreated_at(LocalDateTime created_at)
    {
	this.created_at = created_at;
    }

    public LocalDateTime getUpdated_at()
    {
	return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at)
    {
	this.updated_at = updated_at;
    }

    @Override
    public String toString()
    {
       //Used for logging to be able to show the variables values
       return ReflectionToStringBuilder.toString(this, SHORT_PREFIX_STYLE);
    }
}
