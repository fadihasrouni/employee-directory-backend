package com.postlight.employeedirectory.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class User
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;
    private String fullName;
    private String password;
    @CreationTimestamp
    private Date create_At;
    @UpdateTimestamp
    private Date update_At;

    public User()
    {
    }

    public Long getId()
    {
	return id;
    }

    public void setId(Long id)
    {
	this.id = id;
    }

    public String getUsername()
    {
	return username;
    }

    public void setUsername(String username)
    {
	this.username = username;
    }

    public String getFullName()
    {
	return fullName;
    }

    public void setFullName(String fullName)
    {
	this.fullName = fullName;
    }

    public String getPassword()
    {
	return password;
    }

    public void setPassword(String password)
    {
	this.password = password;
    }

    public Date getCreate_At()
    {
	return create_At;
    }

    public void setCreate_At(Date create_At)
    {
	this.create_At = create_At;
    }

    public Date getUpdate_At()
    {
	return update_At;
    }

    public void setUpdate_At(Date update_At)
    {
	this.update_At = update_At;
    }

    @PrePersist
    protected void onCreate()
    {
	this.create_At = new Date();
    }

    @PreUpdate
    protected void onUpdate()
    {
	this.update_At = new Date();
    }
}
