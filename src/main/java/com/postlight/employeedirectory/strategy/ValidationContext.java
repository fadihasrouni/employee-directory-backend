package com.postlight.employeedirectory.strategy;

public enum ValidationContext
{
    COUNTRY, EMPLOYEE, DEPARTMENT, USER
}
