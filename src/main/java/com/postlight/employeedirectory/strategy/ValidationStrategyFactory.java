package com.postlight.employeedirectory.strategy;

import com.postlight.employeedirectory.strategy.validation.ValidationStrategy;
import com.postlight.employeedirectory.strategy.validation.impl.CountryValidationStrategy;
import com.postlight.employeedirectory.strategy.validation.impl.DepartmentValidationStrategy;
import com.postlight.employeedirectory.strategy.validation.impl.EmployeeValidationStrategy;
import com.postlight.employeedirectory.strategy.validation.impl.UserValidationStrategy;

public class ValidationStrategyFactory
{
    public ValidationStrategy getValidationStrategy(ValidationContext validationContext)
    {
	if (validationContext == null)
	{
	    return null;
	}
	if (validationContext.equals(ValidationContext.COUNTRY))
	{
	    return new CountryValidationStrategy();
	} else if (validationContext.equals(ValidationContext.EMPLOYEE))
	{
	    return new EmployeeValidationStrategy();
	} else if (validationContext.equals(ValidationContext.DEPARTMENT))
	{
	    return new DepartmentValidationStrategy();
	} else if (validationContext.equals(ValidationContext.USER))
	{
	    return new UserValidationStrategy();
	}

	return null;
    }
}
