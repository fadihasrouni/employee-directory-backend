package com.postlight.employeedirectory.strategy.validation;

import org.springframework.validation.BindingResult;

import com.postlight.employeedirectory.controller.request.BaseRequest;

/**
 * This strategy will handle all the validations needed on the controller level
 * 
 * @author Fadi Hasrouni
 *
 */
public interface ValidationStrategy
{
    
    /**
     * Validate the result error and request values when neccessary
     * 
     * @param request
     * @param result
     * @return
     */
    public void validate(BaseRequest request, BindingResult bindingResult);
    
    /**
     * Validate the result error and request values when neccessary
     * 
     * @param request
     * @param result
     * @return
     */
    public void validateLogic(BaseRequest request);
}
