package com.postlight.employeedirectory.strategy.validation.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.postlight.employeedirectory.controller.request.BaseRequest;
import com.postlight.employeedirectory.controller.request.EmployeeRequest;
import com.postlight.employeedirectory.exception.BadRequestException;
import com.postlight.employeedirectory.strategy.validation.ValidationStrategy;

public class EmployeeValidationStrategy implements ValidationStrategy
{

    @Override
    public void validate(BaseRequest request, BindingResult bindingResult)
    {
	// Validate binding result for any errors
	Map<String, String> errorMap = new HashMap<String, String>();
	if (bindingResult.hasErrors())
	{
	    for (FieldError error : bindingResult.getFieldErrors())
	    {
		errorMap.put(error.getField(), error.getDefaultMessage());
	    }

	    throw new BadRequestException("The request has validation errors.", errorMap);
	}

	// A set of validation can be added here
	// Ex: Mobile number
	validateLogic(request);
    }
    
    @Override
    public void validateLogic(BaseRequest request)
    {
	//This method will only validate the logic of the request
	EmployeeRequest employeeRequest = (EmployeeRequest) request;
	
    }

}
