package com.postlight.employeedirectory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class EmployeeDirectoryApplication
{

    @Bean()
    public BCryptPasswordEncoder bCryptPasswordEncoder()
    {
	return new BCryptPasswordEncoder();
    }

    public static void main(String[] args)
    {
	SpringApplication.run(EmployeeDirectoryApplication.class, args);
    }

}
