package com.postlight.employeedirectory.controller.response;

import com.postlight.employeedirectory.model.Country;

public class CountryResponse extends BaseResponse
{
    private Integer id;
    private String name;
    private String alpha2;

    public CountryResponse()
    {

    }

    public CountryResponse(Integer id, String name, String alpha2)
    {
	super();
	this.id = id;
	this.name = name;
	this.alpha2 = alpha2;
    }

    public CountryResponse(Country country)
    {
	super();
	this.id = country.getId();
	this.name = country.getName();
	this.alpha2 = country.getAlpha2();
    }

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public String getAlpha2()
    {
        return alpha2;
    }

    public void setAlpha2(String alpha2)
    {
        this.alpha2 = alpha2;
    }
 
}
