package com.postlight.employeedirectory.controller.response;

import com.postlight.employeedirectory.model.Department;

public class DepartmentResponse extends BaseResponse
{
    private Integer id;
    private String name;
    
    public DepartmentResponse()
    {

    }
    
    public DepartmentResponse(Integer id, String name)
    {
	super();
	this.id = id;
	this.name = name;
    }

    public DepartmentResponse(Department department)
    {
	super();
	this.id = department.getId();
	this.name = department.getName();
    }

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

}
