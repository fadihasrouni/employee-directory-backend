package com.postlight.employeedirectory.controller.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.postlight.employeedirectory.utils.Gender;

public class EmployeeResponse extends BaseResponse
{
    private Integer id;

    private String name;
    private String familyName;
    private String email;
    private Long phoneNumber;
    private String picture;
    private String jobTitle;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    private Gender gender;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date hireDate;
    private float salary;

    private DepartmentResponse department;
    private CountryResponse country;

    public EmployeeResponse()
    {

    }

    public EmployeeResponse(String name, String familyName)
    {
	this.name = name;
	this.familyName = familyName;
    }

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public String getFamilyName()
    {
	return familyName;
    }

    public void setFamilyName(String familyName)
    {
	this.familyName = familyName;
    }

    public String getEmail()
    {
	return email;
    }

    public void setEmail(String email)
    {
	this.email = email;
    }

    public Long getPhoneNumber()
    {
	return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber)
    {
	this.phoneNumber = phoneNumber;
    }

    public String getPicture()
    {
	return picture;
    }

    public void setPicture(String picture)
    {
	this.picture = picture;
    }

    public String getJobTitle()
    {
	return jobTitle;
    }

    public void setJobTitle(String jobTitle)
    {
	this.jobTitle = jobTitle;
    }

    public Date getBirthDate()
    {
	return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
	this.birthDate = birthDate;
    }

    public Gender getGender()
    {
	return gender;
    }

    public void setGender(Gender gender)
    {
	this.gender = gender;
    }

    public Date getHireDate()
    {
	return hireDate;
    }

    public void setHireDate(Date hireDate)
    {
	this.hireDate = hireDate;
    }

    public float getSalary()
    {
	return salary;
    }

    public void setSalary(float salary)
    {
	this.salary = salary;
    }

    public DepartmentResponse getDepartment()
    {
	return department;
    }

    public void setDepartment(DepartmentResponse department)
    {
	this.department = department;
    }

    public CountryResponse getCountry()
    {
	return country;
    }

    public void setCountry(CountryResponse country)
    {
	this.country = country;
    }

}
