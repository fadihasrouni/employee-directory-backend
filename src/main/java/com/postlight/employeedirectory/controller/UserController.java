package com.postlight.employeedirectory.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.postlight.employeedirectory.controller.request.LoginRequest;
import com.postlight.employeedirectory.controller.request.RegisterUserRequest;
import com.postlight.employeedirectory.controller.response.EmployeeResponse;
import com.postlight.employeedirectory.controller.response.JWTLoginSuccessResponse;
import com.postlight.employeedirectory.controller.response.UserResponse;
import com.postlight.employeedirectory.security.JwtTokenProvider;
import com.postlight.employeedirectory.service.UserService;
import com.postlight.employeedirectory.strategy.ValidationContext;
import com.postlight.employeedirectory.strategy.ValidationStrategyFactory;
import com.postlight.employeedirectory.strategy.validation.ValidationStrategy;
import com.postlight.employeedirectory.utils.Constants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
@RequestMapping("/user")
@Api(value="Employee Management System")
public class UserController {

    ValidationStrategyFactory validationFactory = new ValidationStrategyFactory();
    ValidationStrategy validation = validationFactory.getValidationStrategy(ValidationContext.USER);

    @Autowired
    private UserService userService;
    
    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;
    
    @PostMapping(path = "/login", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    @ApiOperation(value = "login user", response = EmployeeResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfull login."),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 406, message = "Cannot login"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public JWTLoginSuccessResponse authenticateUser(@RequestBody @Valid LoginRequest loginRequest, BindingResult result)
    {
	validation.validate(loginRequest, result);

	Authentication authentication = authenticationManager.authenticate(
		new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

	SecurityContextHolder.getContext().setAuthentication(authentication);
	String jwt = Constants.TOKEN_PREFIX + tokenProvider.generateToken(authentication);

	return new JWTLoginSuccessResponse(true, jwt);
    }

    @PostMapping(path = "/register", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    @ApiOperation(value = "Add a new user", response = EmployeeResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully added a new user."),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 406, message = "Cannot add an existing user"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public UserResponse registerUser(@Valid @RequestBody RegisterUserRequest userRequest, BindingResult bindingResult){

	validation.validate(userRequest, bindingResult);

        UserResponse newUser = userService.save(userRequest);

        return newUser;
    }
}