package com.postlight.employeedirectory.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.postlight.employeedirectory.controller.request.EmployeeRequest;
import com.postlight.employeedirectory.controller.response.EmployeeResponse;
import com.postlight.employeedirectory.service.EmployeeService;
import com.postlight.employeedirectory.strategy.ValidationContext;
import com.postlight.employeedirectory.strategy.ValidationStrategyFactory;
import com.postlight.employeedirectory.strategy.validation.ValidationStrategy;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
@RequestMapping("/employee")
@Api(value="Employee Management System")
public class EmployeeController
{
    ValidationStrategyFactory validationFactory = new ValidationStrategyFactory();
    ValidationStrategy validation = validationFactory.getValidationStrategy(ValidationContext.EMPLOYEE);
    
    @Autowired
    EmployeeService employeeService;
    
    @GetMapping(path = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get an employee by ID.", response = EmployeeResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully retrieved employee by ID"),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 404, message = "The employee you were trying to reach is not found"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public EmployeeResponse findEmployeeById(@PathVariable() Integer id)
    {
	EmployeeResponse result = employeeService.findEmployeeById(id);
	return result;
    }
    
    @GetMapping(path = "/list")
    @ResponseBody
    @ApiOperation(value = "Get the list of all employees", response = EmployeeResponse.class, responseContainer = "List")
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully retrieved list of employees"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public List<EmployeeResponse> listAllEmployees()
    {
	List<EmployeeResponse> result = employeeService.listAllEmployees();

	return result;
    }
    
    @PostMapping(path = "/save", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    @ApiOperation(value = "Add a new employee", response = EmployeeResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully added a new employee."),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 406, message = "Cannot add an existing employee"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public EmployeeResponse addEmployee(@RequestBody @Valid EmployeeRequest request, BindingResult bindingResult)
    {
	validation.validate(request, bindingResult);

	EmployeeResponse employeeResponse = employeeService.save(request);

	return employeeResponse;
    }
    
    
    @PostMapping(path = "/update", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    @ApiOperation(value = "Update a existing employee", response = EmployeeResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully updated a new employee."),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 406, message = "Cannot update non-existing employee"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public EmployeeResponse updateEmployee(@RequestBody @Valid EmployeeRequest request, BindingResult bindingResult)
    {
	validation.validateLogic(request);

	EmployeeResponse employeeResponse = employeeService.update(request);

	return employeeResponse;
    }
    
    @DeleteMapping(path = "/delete/{id}")
    @ApiOperation(value = "Delete employee")
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully deleted the employee."),
	    @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
	    @ApiResponse(code = 405, message = "Method not allowed"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public void deleteEmployee(@PathVariable Integer id)
    {
	employeeService.deleteEmployee(id);
    }

}
