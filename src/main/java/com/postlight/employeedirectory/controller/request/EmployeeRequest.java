package com.postlight.employeedirectory.controller.request;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.postlight.employeedirectory.utils.Gender;

import io.swagger.annotations.ApiModelProperty;

public class EmployeeRequest extends BaseRequest
{
    @ApiModelProperty(value = "Values: Empty or -1 when adding. Id of the employee when updating.")
    private Integer id;
    
    @NotBlank(message = "Employee name is required")
    private String name;
    
    private String familyName;
    
    @NotBlank(message = "Employee email is required")
    @Email(message = "Email should be a valid email!")
    private String email;
    
    private Long phoneNumber;
    private String picture;
    
    @NotBlank(message = "Employee job title is required")
    private String jobTitle;
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthDate;
    
    private Gender gender;
    
    @NotNull(message = "Employee hire date is required")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date hireDate;
    
    private float salary;
    @NotNull(message = "Department is required")
    private Integer department;
    @NotNull(message = "Country is required")
    private Integer country;
    
 
  
 

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public String getFamilyName()
    {
	return familyName;
    }

    public void setFamilyName(String familyName)
    {
	this.familyName = familyName;
    }

    public String getEmail()
    {
	return email;
    }

    public void setEmail(String email)
    {
	this.email = email;
    }

    public Long getPhoneNumber()
    {
	return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber)
    {
	this.phoneNumber = phoneNumber;
    }

    public String getPicture()
    {
	return picture;
    }

    public void setPicture(String picture)
    {
	this.picture = picture;
    }

    public String getJobTitle()
    {
	return jobTitle;
    }

    public void setJobTitle(String jobTitle)
    {
	this.jobTitle = jobTitle;
    }

    public Date getBirthDate()
    {
	return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
	this.birthDate = birthDate;
    }

    public Gender getGender()
    {
	return gender;
    }

    public void setGender(Gender gender)
    {
	this.gender = gender;
    }

    public Date getHireDate()
    {
	return hireDate;
    }

    public void setHireDate(Date hireDate)
    {
	this.hireDate = hireDate;
    }

    public float getSalary()
    {
	return salary;
    }

    public void setSalary(float salary)
    {
	this.salary = salary;
    }

    public Integer getDepartment()
    {
	return department;
    }

    public void setDepartment(Integer department)
    {
	this.department = department;
    }

    public Integer getCountry()
    {
	return country;
    }

    public void setCountry(Integer country)
    {
	this.country = country;
    }

}
