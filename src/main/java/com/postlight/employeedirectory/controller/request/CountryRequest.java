package com.postlight.employeedirectory.controller.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;


public class CountryRequest extends BaseRequest
{
    @ApiModelProperty(value = "Values: Empty or -1 when adding. Id of the employee when updating.")
    private Integer id;
    @NotBlank(message = "Country name cannot be blank")
    private String name;
    @NotBlank(message = "Country Alpha 2 code cannot be blank")
    @Size(min = 2, max = 2, message = "The country alpha 2 code must be 2 characters only")
    private String alpha2;

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public String getAlpha2()
    {
        return alpha2;
    }

    public void setAlpha2(String alpha2)
    {
        this.alpha2 = alpha2;
    }
       
}
