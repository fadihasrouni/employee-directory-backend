package com.postlight.employeedirectory.controller.request;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

public class DepartmentRequest extends BaseRequest
{
    @ApiModelProperty(value = "Values: Empty or -1 when adding. Id of the employee when updating.")
    private Integer id;
    @NotBlank(message = "Department name cannot be blank")
    private String name;

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

}
