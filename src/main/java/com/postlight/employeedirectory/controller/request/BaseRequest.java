package com.postlight.employeedirectory.controller.request;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class BaseRequest
{
    @Override
    public String toString()
    {
       //Used for logging to be able to show the variables values
       return ReflectionToStringBuilder.toString(this, SHORT_PREFIX_STYLE);
    }
}
