package com.postlight.employeedirectory.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.postlight.employeedirectory.controller.request.CountryRequest;
import com.postlight.employeedirectory.controller.response.CountryResponse;
import com.postlight.employeedirectory.service.CountryService;
import com.postlight.employeedirectory.strategy.ValidationContext;
import com.postlight.employeedirectory.strategy.ValidationStrategyFactory;
import com.postlight.employeedirectory.strategy.validation.ValidationStrategy;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
@RequestMapping("/country")
@Api(value = "Add/Update/Delete/list countries.")
public class CountryController
{    
    ValidationStrategyFactory validationFactory = new ValidationStrategyFactory();
    ValidationStrategy validation = validationFactory.getValidationStrategy(ValidationContext.COUNTRY);

    @Autowired
    private CountryService countryService;

    @GetMapping(path = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get a country by ID.", response = CountryResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully retrieved country by ID"),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 404, message = "The country you were trying to reach is not found"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public CountryResponse findCountryById(@PathVariable Integer id)
    {	
	CountryResponse result = countryService.findCountryById(id);
	
	return result;
    }
    
    @GetMapping(path = "/list")
    @ResponseBody
    @ApiOperation(value = "Get the list of all countries", response = CountryResponse.class, responseContainer="List")
    @ApiResponses(value =
   { @ApiResponse(code = 200, message = "Successfully retrieved list of countries"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public List<CountryResponse> listAllCountries()
    {
	List<CountryResponse> result = countryService.listAllCountries();
	return result;
    }

    @PostMapping(path = "/save", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    @ApiOperation(value = "Add a new country", response = CountryResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully added a new country."),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 406, message = "Cannot add an existing country"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public CountryResponse addCountry(@RequestBody @Valid CountryRequest request, BindingResult bindingResult)
    {
	validation.validate(request, bindingResult);

	CountryResponse countryResponse = countryService.save(request);

	return countryResponse;
    }

    @PostMapping(path = "/update")
    @ResponseBody
    @ApiOperation(value = "Update an existing country", response = CountryResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully updated the country."),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 406, message = "Cannot update non-existing country"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public CountryResponse updateCountry(@RequestBody @Valid CountryRequest request, BindingResult bindingResult)
    {
	validation.validate(request, bindingResult);

	CountryResponse countryResponse = countryService.update(request);

	return countryResponse;
    }
    
    @DeleteMapping(path = "/delete/{id}")
    @ApiOperation(value = "Delete country")
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully deleted the country."),
	    @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
	    @ApiResponse(code = 405, message = "Method not allowed"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public void deleteCountry(@PathVariable Integer id)
    {
	countryService.deleteCountry(id);
    }
}
