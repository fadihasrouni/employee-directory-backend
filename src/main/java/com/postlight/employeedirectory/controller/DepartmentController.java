package com.postlight.employeedirectory.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.postlight.employeedirectory.controller.request.DepartmentRequest;
import com.postlight.employeedirectory.controller.response.DepartmentResponse;
import com.postlight.employeedirectory.service.DepartmentService;
import com.postlight.employeedirectory.strategy.ValidationContext;
import com.postlight.employeedirectory.strategy.ValidationStrategyFactory;
import com.postlight.employeedirectory.strategy.validation.ValidationStrategy;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
@RequestMapping("/department")
@Api(value = "Add/Update/Delete/list departments.")
public class DepartmentController
{
    ValidationStrategyFactory validationFactory = new ValidationStrategyFactory();
    ValidationStrategy validation = validationFactory.getValidationStrategy(ValidationContext.DEPARTMENT);
    
    @Autowired
    private DepartmentService departmentService;

    @GetMapping(path = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get a department by ID.", response = DepartmentResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully retrieved department by ID"),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 404, message = "The department you were trying to reach is not found"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public DepartmentResponse findDepartmentById(@PathVariable Integer id)
    {	
	DepartmentResponse result = departmentService.findDepartmentById(id);

	return result;
    }
    
    @GetMapping(path = "/list")
    @ResponseBody
    @ApiOperation(value = "Get the list of all departments", response = DepartmentResponse.class, responseContainer="List")
    @ApiResponses(value =
   { @ApiResponse(code = 200, message = "Successfully retrieved list of departments"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public List<DepartmentResponse> listAllDepartments()
    {
	List<DepartmentResponse> result = departmentService.listAllDepartments();
	return result;
    }
    
    @PostMapping(path = "/save", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    @ApiOperation(value = "Add a new department", response = DepartmentResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully added a new department."),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 406, message = "Cannot add an existing department"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public DepartmentResponse addDepartment(@RequestBody @Valid DepartmentRequest request, BindingResult bindingResult)
    {
	validation.validate(request, bindingResult);

	DepartmentResponse departmentResponse = departmentService.save(request);

	return departmentResponse;
    }

    @PostMapping(path = "/update")
    @ResponseBody
    @ApiOperation(value = "Update a existing department", response = DepartmentResponse.class)
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully updated the department."),
	    @ApiResponse(code = 400, message = "Bad Request"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 406, message = "Cannot update non-existing department"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public DepartmentResponse updateDepartment(@RequestBody @Valid DepartmentRequest request, BindingResult bindingResult)
    {
	validation.validate(request, bindingResult);

	DepartmentResponse departmentResponse = departmentService.update(request);

	return departmentResponse;
    }
    
    @DeleteMapping(path = "/delete/{id}")
    @ApiOperation(value = "Delete department")
    @ApiResponses(value =
    { @ApiResponse(code = 200, message = "Successfully deleted the department."),
	    @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
	    @ApiResponse(code = 405, message = "Method not allowed"),
	    @ApiResponse(code = 500, message = "Internal Server Error.") })
    public void deleteDepartment(@PathVariable Integer id)
    {
	departmentService.deleteDepartment(id);
    }
}
