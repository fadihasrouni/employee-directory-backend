package com.postlight.employeedirectory.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.postlight.employeedirectory.model.Country;

/**
 * CountryRepository interface can be implemented for complex queries
 * 
 * @author Fadi Hasrouni
 *
 */
public interface CountryRepository extends Repository<Country, Integer>
{
    /**
     * Find any country by a given ID.
     * 
     * @param id
     * @return
     */
    public Country findById(Integer id);
    
    /**
     * Find any country by Alfa 2 Code.
     * 
     * @param alpha2
     * @return
     */
    public Country findByAlpha2(String alpha2);
    
    /**
     * Saves a new country or updates an existing one
     * 
     * @param country
     * @return
     */
    public Country save(Country country);
    
    /**
     * Get all countries from the database
     * 
     * @return
     */
    public List<Country> findAll();
    
    
    /**
     * Delete a given country
     * 
     * @param country
     */
    public void delete(Country country);
}
