package com.postlight.employeedirectory.repository;

import org.springframework.data.repository.Repository;

import com.postlight.employeedirectory.model.User;

public interface UserRepository  extends Repository<User, Integer>
{
    /**
     * Get user by a given username
     * 
     * @param username
     * @return
     */
    public User findByUsername(String username);
    
    /**
     * Get user by a given id
     * 
     * @param username
     * @return
     */
    public User findById(Long id);
    
    /**
     * Saves a new user or updates an existing one
     * 
     * @param employee
     * @return
     */
    public User save(User employee);

}
