package com.postlight.employeedirectory.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.postlight.employeedirectory.model.Department;

public interface DepartmentRepository extends Repository<Department, Integer>
{
    /**
     * Find any department by a given ID.
     * 
     * @param id
     * @return
     */
    public Department findById(Integer id);
    
    /**
     * Saves a new department or updates an existing one
     * 
     * @param department
     * @return
     */
    public Department save(Department department);
    
    /**
     * Get all departments from the database
     * 
     * @return
     */
    public List<Department> findAll();
    
    
    /**
     * Delete a given department
     * 
     * @param department
     */
    public void delete(Department department);
}
