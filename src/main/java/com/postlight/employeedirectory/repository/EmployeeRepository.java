package com.postlight.employeedirectory.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.postlight.employeedirectory.model.Country;
import com.postlight.employeedirectory.model.Department;
import com.postlight.employeedirectory.model.Employee;

public interface EmployeeRepository extends Repository<Employee, Integer>
{
    /**
     * Find an employee by a given ID.
     * 
     * @param id
     * @return
     */
    public Employee findById(Integer id);
    
    /**
     * Find an employee by a given Department.
     * 
     * @param id
     * @return
     */
    public Employee findByDepartment(Department department);
    
    /**
     * Find an employee by a given country.
     * 
     * @param id
     * @return
     */
    public Employee findByCountry(Country country);
    
    
    /**
     * Get all employees from the database
     * 
     * @return
     */
    public List<Employee> findAll();
    
    /**
     * Saves a new employee or updates an existing one
     * 
     * @param employee
     * @return
     */
    public Employee save(Employee employee);
    
    /**
     * Delete a given employee
     * 
     * @param employee
     */
    public void delete(Employee employee);
}
